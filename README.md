# REDSWARMS

## Overview

As the world becomes more digital, cybersecurity becomes ever more vital. To anticipate this demand in talent, the University of New Hampshire has a cybersecurity club and team that fosters growth in students by training them to face real-world scenarios through competitions. However, building these mock networks for training is currently manual, time-consuming, and error-prone. REDSWARMS is a system designed to facilitate the automatic creation and configuration of small-scale enterprise networks to more quickly perform those security training scenarios and competitions. It features virtual infrastructure using VMWare’s platform for managing a network of virtual machines as well as an automated deployment pipeline with Ansible and Ansible Tower AWX. With REDSWARMS, the cybersecurity team can stand up brand new networks for training in a matter of minutes, allowing students to spend less time configuring and more time learning.

Ready Enterprise Deployment System Which Automates tRaining Modules Securely (REDSWARMS) is a turn-key automated deployment system. This system automatically sets up and configures a network of machines. It is highly configurable and designed to facilitate training with many different networks configured in many different ways. Specific configurations are saveable and archivable so that teams can practice on previous competition network topologies. Scripts should not hard-code any passwords and should observe cybersecurity best practices.

## Requirements

The main goal of the REDSWARMS project is to make setup and configuration of training networks fast and efficient. For future use, the system also needs to be architected in a way that would facilitate simple additions and modifications to team network deployments. Finally, it needs to be well-documented and simple enough to allow new club and team members to be able to simply use the system.

## Support

Check out our [issue tracker](https://unh-redswarms.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=REDSWARMS&view=planning) and [wiki](http://wiki.unhcsc.ccdc/display/CSLN) for more help about using this project.

## Roadmap

View the current sprint progress on Jira: <https://unh-redswarms.atlassian.net/secure/RapidBoard.jspa?rapidView=1&projectKey=REDSWARMS>

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
